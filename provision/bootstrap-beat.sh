#!/usr/bin/env bash

# install ansible (http://docs.ansible.com/intro_installation.html)
apt-get -y install software-properties-common
apt-add-repository -y ppa:ansible/ansible
apt-get update
apt-get -y install ansible

# install beats (https://www.elastic.co/guide/en/beats/libbeat/current/setup-repositories.html)
curl https://packages.elasticsearch.org/GPG-KEY-elasticsearch | sudo apt-key add -
echo "deb https://packages.elastic.co/beats/apt stable main" |  sudo tee -a /etc/apt/sources.list.d/beats.list
apt-get update
apt-get -y install filebeat
sudo update-rc.d filebeat defaults 95 10

# copy examples into /home/vagrant (from inside the logstash node)
cp -a /vagrant/examples/* /home/vagrant
chown -R vagrant:vagrant /home/vagrant

# configure hosts file for our internal network defined by Vagrantfile
cat >> /etc/hosts <<EOL

# vagrant environment nodes
10.0.15.10  logstash
10.0.15.11  kibana
10.0.15.12  graylog2
10.0.15.13  beat
10.0.15.21  elasticsearch1
10.0.15.22  elasticsearch2
10.0.15.23  elasticsearch3
EOL
