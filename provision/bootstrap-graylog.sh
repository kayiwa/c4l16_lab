#!/usr/bin/env bash

# install openjdk
apt-get update
apt-get -y install default-jre

# copy examples into /home/vagrant (from inside the logstash node)
cp -a /vagrant/examples/* /home/vagrant
chown -R vagrant:vagrant /home/vagrant

# configure hosts file for our internal network defined by Vagrantfile
cat >> /etc/hosts <<EOL

# vagrant environment nodes
10.0.15.10  logstash
10.0.15.11  kibana
10.0.15.12  graylog2
10.0.15.13  beat
10.0.15.21  elasticsearch1
10.0.15.22  elasticsearch2
10.0.15.23  elasticsearch3
EOL
